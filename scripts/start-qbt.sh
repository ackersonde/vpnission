#!/bin/bash
searchEngines=/config/qBittorrent/data/nova3/engines
# Check if /config/qBittorrent exists, if not make the directory
if [[ ! -e /config/qBittorrent/config ]]; then
	mkdir -p /config/qBittorrent/config
	mkdir -p $searchEngines
fi
# Set the correct rights accordingly to the PUID and PGID on /config/qBittorrent
chown -R ${PUID}:${PGID} /config/qBittorrent

# Download / install a few search engines
#curl --silent https://raw.githubusercontent.com/BurningMop/qBittorrent-Search-Plugins/main/therarbg.py --output $searchEngines/therarbg.py
#curl --silent https://raw.githubusercontent.com/BurningMop/qBittorrent-Search-Plugins/main/solidtorrents.py --output $searchEngines/solidtorrents.py

##Disabling this because the finished folder is my whole NAS and file ownerships are already managed by ACL.
# Set the rights on the /finished folder
#find /finished -not -user ${PUID} -execdir chown ${PUID}:${PGID} {} \+

# Check qbtUser, change UID/GID if they don't match the environment variables.
qbtUID=$(id -u qbtUser)
qbtGID=$(id -g qbtUser)
if [[ ${PUID} != $qbtUID || ${PGID} != $qbtGID ]]; then
	usermod -u ${PUID} -g ${PGID} qbtUser
fi

# Start qBittorrent
echo "[INFO] Starting qBittorrent daemon..." | ts '%Y-%m-%d %H:%M:%.S'
chmod -R 755 /config/qBittorrent
nohup /bin/bash /entrypoint.sh >/dev/null 2>&1 &

# wait for the entrypoint.sh script to finish and grab the qbittorrent pid
while ! pgrep -f "qbittorrent-nox" >/dev/null; do
	sleep 0.5
done
qbittorrentpid=$(pgrep -f "qbittorrent-nox")

# If the process exists, make sure that the log file has the proper rights and start the health check
if [ -e /proc/$qbittorrentpid ]; then
	echo "[INFO] qBittorrent PID: $qbittorrentpid" | ts '%Y-%m-%d %H:%M:%.S'

	if [[ -e /config/qBittorrent/data/logs/qbittorrent.log ]]; then
		chmod 775 /config/qBittorrent/data/logs/qbittorrent.log
	fi

	# Set some variables that are used
	HOST=${HEALTH_CHECK_HOST}
	DEFAULT_HOST="one.one.one.one"
	INTERVAL=${HEALTH_CHECK_INTERVAL}
	DEFAULT_INTERVAL=300
	DEFAULT_HEALTH_CHECK_AMOUNT=1

	# If host is zero (not set) default it to the DEFAULT_HOST variable
	if [[ -z "${HOST}" ]]; then
		echo "[INFO] HEALTH_CHECK_HOST is not set. For now using default host ${DEFAULT_HOST}" | ts '%Y-%m-%d %H:%M:%.S'
		HOST=${DEFAULT_HOST}
	fi

	# If HEALTH_CHECK_INTERVAL is zero (not set) default it to DEFAULT_INTERVAL
	if [[ -z "${HEALTH_CHECK_INTERVAL}" ]]; then
		echo "[INFO] HEALTH_CHECK_INTERVAL is not set. For now using default interval of ${DEFAULT_INTERVAL}" | ts '%Y-%m-%d %H:%M:%.S'
		INTERVAL=${DEFAULT_INTERVAL}
	fi

	# If HEALTH_CHECK_SILENT is zero (not set) default it to supression
	if [[ -z "${HEALTH_CHECK_SILENT}" ]]; then
		echo "[INFO] HEALTH_CHECK_SILENT is not set. Because this variable is not set, it will be supressed by default" | ts '%Y-%m-%d %H:%M:%.S'
		HEALTH_CHECK_SILENT=1
	fi

	if [ ! -z ${RESTART_CONTAINER} ]; then
		echo "[INFO] RESTART_CONTAINER defined as '${RESTART_CONTAINER}'" | ts '%Y-%m-%d %H:%M:%.S'
	else
		echo "[WARNING] RESTART_CONTAINER not defined,(via -e RESTART_CONTAINER), defaulting to 'yes'" | ts '%Y-%m-%d %H:%M:%.S'
		export RESTART_CONTAINER="yes"
	fi

	# If HEALTH_CHECK_AMOUNT is zero (not set) default it to DEFAULT_HEALTH_CHECK_AMOUNT
	if [[ -z ${HEALTH_CHECK_AMOUNT} ]]; then
		echo "[INFO] HEALTH_CHECK_AMOUNT is not set. For now using default interval of ${DEFAULT_HEALTH_CHECK_AMOUNT}" | ts '%Y-%m-%d %H:%M:%.S'
		HEALTH_CHECK_AMOUNT=${DEFAULT_HEALTH_CHECK_AMOUNT}
	fi
	echo "[INFO] HEALTH_CHECK_AMOUNT is set to ${HEALTH_CHECK_AMOUNT}" | ts '%Y-%m-%d %H:%M:%.S'

	while true; do
		# First wait for the health check interval.
		sleep ${INTERVAL}

		# Confirm the process is still running, start it back up if it's not.
		if ! ps -p $qbittorrentpid >/dev/null; then
			echo "[ERROR] qBittorrent daemon is not running. Restarting..." | ts '%Y-%m-%d %H:%M:%.S'
			nohup /bin/bash /entrypoint.sh >/dev/null 2>&1 &

			# wait for the entrypoint.sh script to finish and grab the qbittorrent pid
			while ! pgrep -f "qbittorrent-nox" >/dev/null; do
				sleep 0.5
			done
			qbittorrentpid=$(pgrep -f "qbittorrent-nox")
		fi
		# Ping uses both exit codes 1 and 2. Exit code 2 cannot be used for docker health checks, therefore we use this script to catch error code 2
		ping -c ${HEALTH_CHECK_AMOUNT} $HOST >/dev/null 2>&1
		STATUS=$?
		if [[ "${STATUS}" -ne 0 ]]; then
			echo "[ERROR] Network is possibly down." | ts '%Y-%m-%d %H:%M:%.S'
			sleep 1
			if [[ ${RESTART_CONTAINER,,} == "1" || ${RESTART_CONTAINER,,} == "true" || ${RESTART_CONTAINER,,} == "yes" ]]; then
				echo "[INFO] Restarting container." | ts '%Y-%m-%d %H:%M:%.S'
				exit 1
			fi
		fi
		if [[ ${HEALTH_CHECK_SILENT,,} == "0" || ${HEALTH_CHECK_SILENT,,} == "false" || ${HEALTH_CHECK_SILENT,,} == "no" ]]; then
			echo "[INFO] Network is up" | ts '%Y-%m-%d %H:%M:%.S'
		fi

		# Check the NAT port forward and update qBittorrent config if there is a change.
		if [[ $ENABLEPROTONVPNPORTFWD -eq 1 ]]; then
			loginData="username=admin&password=geronimo"
			cookie=$(curl -i --silent --data $loginData $WEBUI_URL/api/v2/auth/login | grep "set-cookie" | awk '/set-cookie:/ {print $2}' | sed 's/;//') >/dev/null 2>&1
			if [[ $cookie ]]; then
				setPort=$(curl --silent $WEBUI_URL/api/v2/app/preferences --cookie $cookie | jq '.listen_port') >/dev/null 2>&1
				currentPort=$(natpmpc -a 1 0 udp 60 -g 10.2.0.1 | grep "public port" | awk '/Mapped public port/ {print $4}')
				if [[ $setPort -ne $currentPort ]]; then
					portData="json={\"listen_port\":$currentPort}"
					curl -i --silent --data $portData $WEBUI_URL/api/v2/app/setPreferences --cookie $cookie >/dev/null 2>&1
				fi
				curl --silent -X 'POST' "$WEBUI_URL/api/v2/auth/logout" -H 'accept: */*' -d '' --cookie $cookie >/dev/null 2>&1
			else
				echo "[WARNING] Unable to log into the web UI." | ts '%Y-%m-%d %H:%M:%.S'
			fi
			unset cookie
		fi
	done
else
	echo "[ERROR] qBittorrent failed to start!" | ts '%Y-%m-%d %H:%M:%.S'
fi
