[![pipeline status](https://gitlab.com/ackersonde/vpnission/badges/main/pipeline.svg)](https://gitlab.com/ackersonde/vpnission/-/commits/main)

# vpnission

Wireguard + Transmission <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Transmission_Icon.svg/64px-Transmission_Icon.svg.png" width="16"> running in one, easy to customize and redeploy docker container.

# Build + Deploy

It's turtles all the way down: as this runs on one of my home raspberry PIs, the Github Action needs to prepare the deployment and execute it. The instance running, in turn, has access to my (IPv6) home network where it ultimately redeploys the correctly configured docker container.

Because of the additional "hops", the [environment variables](.gitlab-ci.yml#L36) also need to be carefully pushed around for correct operation.

Rebuilding the docker image is necessary if/when my VPN service changes requirements or you need to rework some of the internal bells & whistles (media server requirements, etc.) or it's time to use latest version of OS + software.

Used everyday by my [bender slackbot](https://gitlab.com/ackersonde/bender-slackbot/).

NOTE: Jellyfin is configured to use NVIDIA cuda GPU encoding. This requires a [compatible video card](https://developer.nvidia.com/video-encode-and-decode-gpu-support-matrix-new) and following the instructions here: https://jellyfin.org/docs/general/administration/hardware-acceleration/nvidia#configure-with-linux-virtualization

Should also work with K8s.

## TODO

https://github.com/ngosang/trackerslist?tab=readme-ov-file

https://github.com/Jorman/Scripts/blob/master/AddqBittorrentTrackers.sh
